
public class BOOK {
	
	String name;
	String bookType;
	int quantity;
	
	
	public BOOK(String name,String bookType,int quantity) {
		this.name = name;
		this.bookType = bookType;
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
	
}
