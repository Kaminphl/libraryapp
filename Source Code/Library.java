import java.util.ArrayList;
import java.util.Scanner;

public class Library {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<BOOK> book = new ArrayList<BOOK>();
		ArrayList<USER> user = new ArrayList<USER>();
		
		int numbertype = 0;
		int numbercommand = 0;
		int command = 0;
		int userid = -1 ;
		Scanner a = new Scanner(System.in);
		Scanner b = new Scanner(System.in);
		
		do {
			System.out.println(" |---- Welcome To Library Room ----| ");
			System.out.println("1) User ");
			System.out.println("2) Admin ");
			System.out.println("3) Exit ");
			System.out.println(" |---------------------------------| ");
			System.out.print(" Insert Number To Login: ");
			numbertype = a.nextInt();
			
			do {
				
				if(numbertype == 1){
					
					if(userid == -1){
						for(int i = 0;i < user.size();i++){
							System.out.println( "|---- All User ----|" );
							System.out.println( (i+1)+": "+user.get(i).getFirstName()+" "+user.get(i).getLastName()+" Role: "+user.get(i).getRole() );
							System.out.println( "|------------------|" );
							System.out.println( "Insert Numeber To Login: " );
							int check = a.nextInt();
							userid = check-1;
						}
					}
					
					System.out.println("--- Welcome Library App(User) ---");
					System.out.println("1) Borrow a Book");
					System.out.println("2) Return a Book");
					System.out.println("3) Exit");
					System.out.print("Insert Number: ");
					numbercommand = a.nextInt();
					
					if(numbercommand == 1) {
						if( book.size()!=0 ) {
							for(int i = 0;i < book.size();i++){
								System.out.println( "|---- All Book ----|" );
								System.out.println( (i+1)+": "+book.get(i).getName()+" Type: "+book.get(i).getBookType()+" Quantity: "+book.get(i).getQuantity() );
								System.out.println( "|------------------|" );
							}
							System.out.print("Insert Number To Borrow Book: ");
							int choice = a.nextInt();
							book.get(choice-1).setQuantity( book.get(choice-1).getQuantity()-1 );

							BOOK borrow = new BOOK(book.get(choice-1).getName() ,book.get(choice-1).getBookType() , book.get(choice-1).getQuantity());
							user.get(userid).UserBorrow.add(borrow);
							
						}else {
							System.out.println( "Don't Have Book" );
						}
						
					}else if(numbercommand == 2){
						
						if( user.get(userid).UserBorrow.size()!=0 ) {
							for(int i = 0;i < user.get(userid).UserBorrow.size() ;i++){
								System.out.println( "|---- All Book ----|" );
								System.out.println( (i+1)+": "+user.get(userid).UserBorrow.get(i).getName()+" Type: "+user.get(userid).UserBorrow.get(i).getBookType()+" Quantity: "+user.get(userid).UserBorrow.get(i).getQuantity() );
								System.out.println( "|------------------|" );
							}
							System.out.print("Insert Number To Return a Book: ");
							int choice = a.nextInt();
							book.get(choice-1).setQuantity( book.get(choice-1).getQuantity()+1 );
							user.get(userid).UserBorrow.remove(choice-1);
							
						}else {
							System.out.println( "Don't Have Borrow Book" );
						}
						
					}
					
					
				}else if(numbertype == 2){
					// ADMIN
					
					System.out.println("--- Welcome Library App(Admin) ---");
					System.out.println("1) TOPIC BOOK");
					System.out.println("2) TOPIC USER");
//					System.out.println("3) REPORT");
					System.out.println("3) Exit");
					System.out.print("Insert Number: ");
					numbercommand = a.nextInt();
//					
					if(numbercommand == 1) {
						
						System.out.println("--- TOPIC BOOK ---");
						System.out.println("1) ADD BOOK");
						System.out.println("2) DELETE BOOK");
						System.out.println("3) EDIT BOOK");
						System.out.println("4) Exit");
						System.out.print("Insert Number: ");
						command = a.nextInt();
						
						
						if(command == 1) {
							
							System.out.print("Insert BooK Name: ");
							String name = b.nextLine();
							System.out.print("Insert BookType: ");
							String Type = b.nextLine();
							System.out.print("Insert Quantity: ");
							int Quantity = a.nextInt();
							BOOK ABK = new BOOK(name,Type,Quantity);
							book.add(ABK);
								
						}else if(command == 2){
							if( book.size()!=0 ) {
								for(int i = 0;i < book.size();i++){
									System.out.println( "|---- All Book ----|" );
									System.out.println( (i+1)+": "+book.get(i).getName()+" Type: "+book.get(i).getBookType()+" Quantity: "+book.get(i).getQuantity() );
									System.out.println( "|------------------|" );
								}
								System.out.print("Insert Number To Delete Book: ");
								int choice = a.nextInt();
								book.remove(choice-1);
							}else {
								System.out.println( "Don't Have Book" );
							}
							
						}else if(command == 3){
							
							if( book.size()!=0 ) {
								for(int i = 0;i < book.size();i++){
									System.out.println( "|---- All Book ----|" );
									System.out.println( (i+1)+": "+book.get(i).getName()+" Type: "+book.get(i).getBookType()+" Quantity: "+book.get(i).getQuantity() );
									System.out.println( "|------------------|" );
								}
								System.out.print("Insert Number To Edit Book: ");
								int choice = a.nextInt();
								System.out.print("Insert Book Name: ");
								String name = b.nextLine();
								book.get(choice-1).setName(name);
								System.out.print("Insert Book Type: ");
								String type = b.nextLine();
								book.get(choice-1).setBookType(type);
								System.out.print("Insert Book Quantity: ");
								int Quantity = b.nextInt();
								book.get(choice-1).setQuantity(Quantity);
								
							}else {
								System.out.println( "Don't Have Book" );
							}
														
						}
						
						
						
						
											
					}else if(numbercommand == 2){

						System.out.println("--- TOPIC USER (Admin) ---");
						System.out.println("1) ADD USER");
						System.out.println("2) DELETE USER");
						System.out.println("3) EDIT USER");
						System.out.println("4) Exit");
						System.out.print("Insert Number: ");
						command = a.nextInt();
						
						
						if(command == 1) {
							
							System.out.print("Insert User FirstName: ");
							String first = b.nextLine();
							System.out.print("Insert User LastName: ");
							String last = b.nextLine();
							System.out.print("Insert User Role: ");
							String role = b.nextLine();
							USER userL = new USER(first,last,role);
							user.add(userL);
								
						}else if(command == 2){
							
							if( user.size()!=0 ) {
								for(int i = 0;i < user.size();i++){
									System.out.println( "|---- All User ----|" );
									System.out.println( (i+1)+": "+user.get(i).getFirstName()+" Type: "+user.get(i).getLastName() + " Role: " +user.get(i).getRole() );
									System.out.println( "|------------------|" );
								}
								System.out.print("Insert Number To Delete USER: ");
								int choice = a.nextInt();
								user.remove(choice-1);
							}else {
								System.out.println( "Don't Have User" );
							}
							
						}else if(command == 3){
							
							if( user.size()!=0 ) {
								for(int i = 0;i < user.size();i++){
									System.out.println( "|---- All User ----|" );
									System.out.println( (i+1)+": "+user.get(i).getFirstName()+" Type: "+user.get(i).getLastName() + " Role: " +user.get(i).getRole() );
									System.out.println( "|------------------|" );
								}
								System.out.print("Insert Number To Edit USER: ");
								int choice = a.nextInt();
								System.out.print("Insert User FirsrtName: ");
								String name = b.nextLine();
								book.get(choice-1).setName(name);
								System.out.print("Insert Book Type: ");
								String type = b.nextLine();
								book.get(choice-1).setBookType(type);
								
							}else {
								System.out.println( "Don't Have User" );
							}
														
						}
						
					}
						
				}
				
			}while(numbercommand != 3 );
			
			userid = -1;
			
		}while(numbertype != 3 );
	}

}
